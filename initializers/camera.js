'use strict';
const {Initializer, api} = require('actionhero');
const { Camera } = require('../lib/camera/Camera');

module.exports = class CameraInitializer extends Initializer {
  constructor() {
    super();
    this.name = 'camera';
    this.loadPriority = 1100;
    this.startPriority = 1100;
    this.stopPriority = 1100;
  }

  async initialize() {
    if (api.mode === 'PAYLOAD') {
      api.log(`${this.loadPriority} ${this.name} initializing`);
      api['camera'] = new Camera({
        output: api.config.camera.photoOutputDir,
        rotation: api.config.camera.cameraRotation
      });
    }
  }

  async start() {}
  async stop() {}
};
