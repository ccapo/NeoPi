'use strict';
const ActionHero = require('actionhero');

module.exports = class HeartbeatInitializer extends ActionHero.Initializer {
  constructor() {
    super();
    this.name = 'heartbeat';
    this.loadPriority = 1000;
    this.startPriority = 1000;
    this.stopPriority = 1000;
  }

  async initialize() {
    ActionHero.api['heartbeat'] = {
      readings: {
        data: [],
        maxDataLength: 10
      }
    };
  }

  async start() {}
  async stop() {}
};
