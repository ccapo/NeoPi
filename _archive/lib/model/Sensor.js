'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sensor', {
    name: {
      type: DataTypes.STRING,
      field: 'name'
    }
  }, {
    freezeTableName: true,
    timestamps: false,
    indexes: [
      {
        unique: true,
        method: 'BTREE',
        fields: ['id']
      }
    ]
  });
};
