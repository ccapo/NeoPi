'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reading', {
    timestamp: {
      type: DataTypes.DATE,
      field: 'timestamp'
    },
    sensor_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'sensor',
        key: 'id'
      }
    }
  }, {
    freezeTableName: true,
    timestamps: false,
    indexes: [
      {
        unique: true,
        method: 'BTREE',
        fields: ['id']
      }
    ]
  });
};
