'use strict';

const SG90 = require('../servo/SG90').SG90;
const Camera = require('../camera/Camera').Camera;
const SleepMS = 10;

class Armature {
  constructor(options) {
    this._pan = new SG90({
      pin: options.panPin
    });

    this._tilt = new SG90({
      pin: options.tiltPin
    });

    this._camera = new Camera({
      output: options.photoOutputDir,
      rotation: options.cameraRotation
    });
  }

  _init() {
    return Promise.all([
      this._pan._init(),
      this._tilt._init(),
    ]);
  }

  _shutdown() {
    this._pan._shutdown();
    this._tilt._shutdown();
  }

  runProgram(program, captureImage = true) {
    return new Promise((resolve, reject) => {
      this._pan.move(program.pan);
      this._tilt.move(program.tilt);

      if (captureImage) {
        setTimeout(() => {
          this._camera.capture().then(imageFileName => {
            resolve(imageFileName);
          });
        }, SleepMS);
      } else {
        resolve();
      }
    });
  }
}

module.exports.Armature = Armature;
