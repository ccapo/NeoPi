'use strict';

const EventEmitter = require('events').EventEmitter;
const Bluebird = require('bluebird');
const SerialPort = require('serialport');
const Validator = require('is-my-json-valid');

const XBEE_COMMAND_DELAY = 1200;
const MAXIMUM_RECEIVE_BUFFER_SIZE = 512;

let validators = {
  program: Validator({
    required: true,
    type: 'array',
    items: {
      type: 'object',
      additionalProperties: false,
      properties: {
        cmd: {
          type: 'string'
        },
        arg: {
          type: 'string'
        }
      },
      required: ['cmd', 'arg']
    }
  }, {
    greedy: true
  })
};

class XBee {
  constructor(device, baudRate = 9600) {
    if (!device) {
      this._device = this._getDeviceName();
    } else {
      this._device = device;
    }

    this._baudRate = baudRate;
    this._port = null;
    this._buffer = '';
    this._log = console.log;
    this.events = new EventEmitter();
    this._commandMode = false;
  }

  _getDeviceName() {
    switch (process.env.MODE) {
      case 'MISSIONCONTROL': {
        this._device = `/dev/ttyAMA0`;
      } break;

      case 'PAYLOAD': {
        this._device = `/dev/cu.usbserial-DN02N149`;
      } break;

      default: {
        throw new Error(`invalid MODE=${process.env.MODE}`);
      } break;
    }
  }

  async initialize() {
    this._port = Bluebird.promisifyAll(new SerialPort(this._device, {
      baudRate: this._baudRate,
      autoOpen: false
    }));

    await this._port.openAsync();
  }

  validateProgram(program) {
    validators.program(program);
    if (validators.program.errors) {
      throw new Error(`Program error: ${validators.program.errors.field} ${validators.program.errors.message}`);
    }
  }

  async _serialRunProgramEntries(program) {
    try {
      while (program.length > 0) {
        let entry = program.shift();
        await this.command(entry.cmd, entry.arg);
      }
    } catch (e) {
      console.trace(e);
      throw e;
    }
  }

  async run(program) {
    try {
      this.validateProgram(program);
      await this.enterCommandMode();
      await this.exitCommandMode();
    } catch (e) {
      console.trace(e);
      throw e;
    }
  }

  async close() {
    await this._port.drainAsync();
    await this.pause(100);
    await this._port.closeAsync();
  }

  async delay(ms) {
    await Promise.delay(ms);
  }

  async pause() {
    this._port.pause();
  }

  async resume() {
    this._port.resume();
  }

  onOpen() {
    this._log(`${this._device}::open`);
  }

  onData(data = '') {
    // If we're in command mode, ignore data events
    if (this._commandMode === true) {
      return;
    }

    // Sometimes the data contains 'OK'
    if (data[0] === 'O' && data[1] === 'K') {
      data = data.substring(2);
    }

    // Sometimes the buffer contains 'OK'
    if (this._buffer[0] === 'O' && this._buffer[1] === 'K') {
      this._buffer = this._buffer.substring(2);
    }

    // Clean up the incoming string
    data = data.toString().trim().replace('\n', '');

    // Append the data to our buffer
    this._buffer += data;

    if (this._buffer.length > MAXIMUM_RECEIVE_BUFFER_SIZE) {
      let packetCount = this._buffer.split('~').length;
      if (packetCount > 1) {
        console.log(`Found ${packetCount} packets!`);
      }

      console.log(`ALERT: Receive buffer is ${this._buffer.length} bytes long; resetting...`);
      // console.log(this._buffer);
      this._buffer = '';
    }

    if (this._buffer[this._buffer.length - 1] === '}') {
      let messages = this._buffer.replace(/}{/g, '}\n{').split('\n');
      for (const message of messages) {
        try {
          this.events.emit('data', JSON.parse(message));
        } catch (e) {
          console.log(e.message);
          console.log(this._buffer);
        }
      }
      this._buffer = '';
    }
  }

  onClose() {
    this._log(`${this._device}::close`);
  }

  onError(err) {
    this._log(`${this._device}::error ${err.message}`);
  }

  onDisconnect(err) {
    this._log(`${this._device}::disconnect ${err.message}`);
  }

  async writeRaw(data = '', crlf = true) {
    return await this._port.writeAsync(crlf ? data + '\r' : data);
  }

  readRaw(size = null) {
    return this._port.read(size);
  }

  async sendBuffer(buffer) {
    return await this._port.writeAsync(buffer, 'binary');
  }

  async enterCommandMode() {
    this._commandMode = true;
    await this.command('+++');
  }

  async exitCommandMode() {
    this._commandMode = false;
    await this.command('ATCN', '');
    await this.resume();
  }

  async command(cmd, arg) {
    let verifyResponse = arg;
    let cmdMode = false;
    if (cmd === '+++') {
      cmdMode = true;
      verifyResponse = 'OK';
    }

    if (cmdMode) {
      await this.delay(1000);
    }

    await this.pause();
    console.log(`${cmd} ${arg ? arg : ''}`);

    if (cmdMode) {
      await this.writeRaw(cmd, false);
    }

    await this.writeRaw(`${cmd} ${arg ? arg : ''}`, true);
    await this.delay(XBEE_COMMAND_DELAY);

    let buffer = await this.readRaw();
    if (!buffer) {
      throw new Error(`XBee may require a restart; ${cmd} returned ${buffer}!`);
    }

    let ok = buffer.toString().trim();
    if (ok.indexOf('OK') === -1) {
      throw new Error(`Expected OK but got ${ok}`);
    }

    console.log(ok.replace('OK', ''));

    // Skip verification of certain commands
    if (['ATCN', 'ATRE', 'ATWR'].indexOf(cmd) !== -1) return;
    await this.verifyCommand(cmd, verifyResponse);
  }

  async verifyCommand(cmd, arg) {
    let cmdMode = false;
    if (cmd === '+++') {
      cmdMode = true;
    }

    if (cmdMode) {
      await this.writeRaw('AT', true);
    }

    await this.writeRaw(`${cmd}`, true);
    await this.delay(XBEE_COMMAND_DELAY);

    let buffer = await this.readRaw();
    let response = buffer.toString().trim();
    if (response !== arg) {
      throw new Error(`Expected ${arg} but got ${response}`);
    }
  }
}

module.exports.XBee = XBee;
