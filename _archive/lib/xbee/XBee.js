'use strict';

const EventEmitter = require('events').EventEmitter;
const Bluebird = require('bluebird');
const SerialPort = require('serialport');
const Validator = require('is-my-json-valid');
const whileLoop = require('promise-while-loop');

const XBEE_COMMAND_DELAY = 1200;
const MAXIMUM_RECIEVE_BUFFER_SIZE = 512;

let validators = {
  program: Validator({
    required: true,
    type: 'array',
    items: {
      type: 'object',
      additionalProperties: false,
      properties: {
        cmd: {
          type: 'string'
        },
        arg: {
          type: 'string'
        }
      },
      required: ['cmd', 'arg']
    }
  }, {
    greedy: true
  })
};

class XBee {
  constructor(device, baudRate = 9600) {
    this._device = device;
    this._baudRate = baudRate;
    this._port = null;
    this._buffer = '';
    this._log = console.log;
    this.events = new EventEmitter();
    this._commandMode = false;
  }

  initialize() {
    this._port = Bluebird.promisifyAll(new SerialPort(this._device, {
      baudRate: this._baudRate,
      autoOpen: false
    }));

    this._port.on('open',       this.onOpen.bind(this));
    this._port.on('data',       this.onData.bind(this));
    this._port.on('close',      this.onClose.bind(this));
    this._port.on('error',      this.onError.bind(this));
    this._port.on('disconnect', this.onDisconnect.bind(this));

    return this._port.openAsync();
  }

  get commandMode() {
    return this._commandMode;
  }

  set commandMode(value) {
    this._commandMode = value;
  }

  validateProgram(program) {
    validators.program(program);
    if (validators.program.errors) {
      throw new Error(`Program error: ${validators.program.errors.field} ${validators.program.errors.message}`);
    }
  }

  _serialRunProgramEntries(program) {
    return new Promise((resolve, reject) => {
      return whileLoop(() => {
        // predicate
        return program.length > 0;
      }, () => {
        // func
        let entry = program.shift();
        return this.command(entry.cmd, entry.arg);
      }).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }

  run(program) {
    return new Promise((resolve, reject) => {
      this.validateProgram(program);
      return this.enterCommandMode().then(() => {
        return this._serialRunProgramEntries(program);
      }).then(() => {
        return this.exitCommandMode();
      }).then(() => {
        resolve();
      }).catch(err => {
        console.trace(err);
        reject(err);
      });
    });
  }

  close() {
    return this._port.drainAsync().then(() => {
      return this.pause(100).then(() => {
        return this._port.closeAsync();
      });
    });
  }

  delay(ms) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
      }, ms);
    });
  }

  pause() {
    return Promise.resolve().then(() => {
      this._port.pause();
    });
  }

  resume() {
    return Promise.resolve().then(() => {
      this._port.resume();
    });
  }

  onOpen() {
    this._log(`${this._device}::open`);
  }

  onData(data = '') {
    // If we're in command mode, ignore data events
    if (this._commandMode === true) {
      return;
    }

    // Sometimes the data contains 'OK'
    if (data[0] === 'O' && data[1] === 'K') {
      data = data.substring(2);
    }

    // Sometimes the buffer contains 'OK'
    if (this._buffer[0] === 'O' && this._buffer[1] === 'K') {
      this._buffer = this._buffer.substring(2);
    }

    // Clean up the incoming string
    data = data.toString().trim().replace('\n', '');

    // Append the data to our buffer
    this._buffer += data;

    if (this._buffer.length > MAXIMUM_RECIEVE_BUFFER_SIZE) {

      let packetCount = this._buffer.split('~').length;
      if (packetCount > 1) {
        console.log(`Found ${packetCount} packets!`);
      }

      console.log(`ALERT: Receive buffer is ${this._buffer.length} bytes long; resetting...`);
      // console.log(this._buffer);
      this._buffer = '';
    }

    if (this._buffer[this._buffer.length - 1] === '}') {
      let messages = this._buffer.replace(/}{/g, '}\n{').split('\n');
      for (const message of messages) {
        try {
          this.events.emit('data', JSON.parse(message));
        } catch (e) {
          console.log(e.message);
          console.log(this._buffer);
        }
      }
      this._buffer = '';
    }
  }

  onClose() {
    this._log(`${this._device}::close`);
  }

  onError(err) {
    this._log(`${this._device}::error ${err.message}`);
  }

  onDisconnect(err) {
    this._log(`${this._device}::disconnect ${err.message}`);
  }

  writeRaw(data = '', crlf = true) {
    return this._port.writeAsync(crlf ? data + '\r' : data);
  }

  readRaw(size = null) {
    return this._port.read(size);
  }

  sendBuffer(buffer) {
    return this._port.writeAsync(buffer, 'binary');
  }

  flush() {
    return this._port.flushAsync();
  }

  enterCommandMode() {
    this._commandMode = true;
    return this.command('+++');
  }

  exitCommandMode() {
    this._commandMode = false;
    return this.command('ATCN', '').then(() => {
      return this.resume();
    });
  }

  command(cmd, arg) {
    let verifyResponse = arg;
    let cmdMode = false;
    if (cmd === '+++') {
      cmdMode = true;
      verifyResponse = 'OK';
    }
    return Promise.resolve().then(() => {
      if (cmdMode) {
        return this.delay(1000);
      }
    }).then(() => {
      return this.pause();
    }).then(() => {
      console.log(`${cmd} ${arg ? arg : ''}`);
      if (cmdMode) {
        return this.writeRaw(cmd, false);
      }
      return this.writeRaw(`${cmd} ${arg ? arg : ''}`, true);
    }).then(() => {
      return this.delay(XBEE_COMMAND_DELAY);
    }).then(() => {
      return this.readRaw();
    }).then((buffer) => {
      if (!buffer) {
        throw new Error(`XBee may require a restart; ${cmd} returned ${buffer}!`);
      }

      let ok = buffer.toString().trim();
      if (ok.indexOf('OK') !== -1) {
        ok.replace('OK', '');
        console.log(ok);
        return ok;
      }
      throw new Error(`Expected OK but got ${ok}`);
    }).then(() => {
      // Skip verification of certain commands
      if (['ATCN', 'ATRE', 'ATWR'].indexOf(cmd) !== -1) return;
      return this.verifyCommand(cmd, verifyResponse);
    });
  }

  verifyCommand(cmd, arg) {
    let cmdMode = false;
    if (cmd === '+++') {
      cmdMode = true;
    }
    return Promise.resolve().then(() => {
      if (cmdMode) {
        return this.writeRaw('AT', true);
      }
      return this.writeRaw(`${cmd}`, true);
    }).then(() => {
      return this.delay(XBEE_COMMAND_DELAY);
    }).then(() => {
      return this.readRaw();
    }).then(buffer => {
      let response = buffer.toString().trim();
      if (response === arg) {
        return;
      }
      throw new Error(`Expected ${arg} but got ${response}`);
    });
  }
}

module.exports.XBee = XBee;

function test() {
  const FrameBuilder = require('./frame/FrameBuilder').FrameBuilder;

  let dev = `/dev/ttyAMA0`;
  let xbee = new XBee(dev, 115200);

  let fb = new FrameBuilder();
  let buffer = fb.build(0x10, {
    destination64: '0013A200415B3CA7',
    destination16: 'fffe',
    data: '0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345'
  });
  return xbee.initialize().then(result => {
    return Promise.resolve().then(() => {
      return xbee.enterCommandMode();
    }).then(() => {
      return xbee.exitCommandMode();
    }).then(() => {
      let ops = [];

      for (let i = 0; i < 10; i++) {
        ops.push(xbee.writeRaw(buffer.toString('hex')));
      }

      return Promise.all(ops);
    });
  }).catch(e => {
    console.trace(e);
  });
}
