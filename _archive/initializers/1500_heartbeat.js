'use strict';

module.exports = {
  loadPriority:  1500,
  startPriority: 1500,
  stopPriority:  1500,
  initialize: function(api, next) {
    api.heartbeat = {
      readings: {
        data: [],
        maxDataLength: 10
      }
    };

    next();
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
