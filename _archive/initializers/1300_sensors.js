'use strict';

const Promise = require('bluebird');

module.exports = {
  loadPriority:  1300,
  startPriority: 1300,
  stopPriority:  1300,
  initialize: function(api, next) {
    try {
      switch (api.mode) {
        case 'PAYLOAD': {
          api.log(`Sensor startup`);

          const LIS3DH   = require('../lib/sensor/LIS3DH').LIS3DH;
          const HMC5883L = require('../lib/sensor/HMC5883L').HMC5883L;
          const BME280   = require('../lib/sensor/BME280').BME280;
          const TSL2591  = require('../lib/sensor/TSL2591').TSL2591;
       // const DS18B20  = require('../lib/sensor/DS18B20').DS18B20;
          const SI1145   = require('../lib/sensor/SI1145').SI1145;
          const GPS      = require('../lib/sensor/GPS').GPS;

          api.sensors = {
            Accelerometer: new LIS3DH({db: api.database, cache: api.cache}),
            Compass:       new HMC5883L({db: api.database, cache: api.cache}),
            Environment:   new BME280({db: api.database, cache: api.cache}),
            Luminosity:    new TSL2591({db: api.database, cache: api.cache}),
         // Temperature:   new DS18B20({db: api.database, cache: api.cache}),
            Ultraviolet:   new SI1145({db: api.database, cache: api.cache}),
            GPS:           new GPS({db: api.database, cache: api.cache})
          };

          for (const key of Object.keys(api.sensors)) {
            let sensor = api.sensors[key];
            sensor.events.on('reading', reading => {
              api.tasks.enqueue('transmitRadioData', reading, 'default', err => {
                if (err) {
                  api.log(`Error enqueueing task 'transmitRadioData'`, 'error', err);
                }
              });
            });
          }

          api.sensors = Object.assign(api.sensors, {
            LIS3DH:   api.sensors.Accelerometer,
            HMC5883L: api.sensors.Compass,
            BME280:   api.sensors.Environment,
            TSL2591:  api.sensors.Luminosity,
         // DS18B20:  api.sensors.Temperature,
            SI1145:   api.sensors.Ultraviolet,
            GPS:      api.sensors.GPS
          });

          api.readings = {
            ir:            0,
            visibility:    0,
            uv:            0,
            heading:       0,
            temperature:   0,
            humidity:      0,
            pressure:      0,
            altitude:      0,
            accelX:        0,
            accelY:        0,
            accelZ:        0,
            latitude:      0,
            longitude:     0,
            altitude_gps:  0,
            external_temp: 0

          };

          const Sensor = require('../lib/core/Sensor').Sensor;
          api.writeSensor = new Sensor({
            db: api.database,
            cache: api.cache
          });

          let sensors = Object.keys(api.sensors);
          Promise.each(sensors, (item, i) => {
            let sensor = api.sensors[item];
            api.log(`Initializing ${item}: ${sensor.constructor.name}`);
            return sensor._init().catch(err => {
              console.trace(err);
              throw err;
            });
          }).then(result => {
            console.log(result);
            next();
          }).catch(err => {
            console.trace(err);
            next(err);
          });
        } break;

        case 'MISSIONCONTROL': {
          let sensors = [
            'LIS3DH',
            'HMC5883L',
            'BME280',
            'TSL2591',
            'SI1145',
            'DS18B20',
            'GPS',
            'heartbeat',
            'system'
          ];

          let ops = [];

          for (const sensor of sensors) {
            let roomName = `/sensor/${sensor.toLowerCase()}`;
            api.log(`Adding channel ${roomName} ...`);
            ops.push(api.chatRoom.addAsync(roomName).catch(err => {
              if (err.message === 'room exists') {
                return;
              }
              throw err;
            }));
          }

          Promise.all(ops).then(() => {
            next();
          }).catch(err => {
            next(err);
          });
        } break;

        default: {
          next();
        }
      }
    } catch (e) {
      console.trace(e);
      next(e);
    }
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
