// 'use strict';
//
// module.exports = {
//   loadPriority:  1259,
//   startPriority: 1259,
//   stopPriority:  1259,
//   initialize: function(api, next) {
//     switch (api.mode) {
//       case 'PAYLOAD': {
//         api.log(`GPSD startup`);
//         const Daemon = require('node-gpsd').Daemon;
//         api.gpsd = new Daemon({
//           program: 'gpsd',
//           device: '/dev/ttyAMA0',
//           port: 12947,
//           pid: './pids/gpsd.pid',
//           readOnly: false,
//           logger: {
//             info: function(data) {
//               api.log(`[GPSD] ${data}`, 'notice');
//             },
//             warn: function(data) {
//               api.log(`[GPSD] ${data}`, 'warn');
//             },
//             error: function(data) {
//               api.log(`[GPSD] ${data}`, 'error');
//             }
//           }
//         });
//
//         api.gpsd.start(() => {
//           next();
//         });
//       } break;
//
//       default: {
//         next();
//       } break;
//     }
//   },
//   start: function(api, next) {
//     next();
//   },
//   stop: function(api, next) {
//     next();
//   }
// };
