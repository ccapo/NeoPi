'use strict';

/* eslint no-unused-vars:0 */
const SQLite = require('sqlite3').verbose();
const Sequelize = require('sequelize');
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp-promise');
const sprintf = require('sprintf-js').sprintf;

function loadModels(sequelize) {
  return new Promise((resolve, reject) => {
    try {
      let baseDir = path.join(__dirname, '../lib/model/');
      let files = fs.readdirSync(baseDir);
      for (const file of files) {
        if (fs.lstatSync(path.join(baseDir, file)).isFile()) {
          sequelize.import(path.join(__dirname, '../lib/model/', file));
        }
      }

      let models = Object.keys(sequelize.models);

      let ops = [];
      for (const model of models) {
        ops.push(sequelize.models[model].sync());
      }

      return Promise.all(ops).then(() => {
        return resolve();
      });
    } catch (e) {
      console.trace(e);
      return reject(e);
    }
  });
}

function initDatabase() {
  return new Promise((resolve, reject) => {
    return mkdirp(path.join(__dirname, '../data')).then(result => {
      let now = new Date();

      let filename = sprintf('neopi.%04d%02d%02d-%02d%02d%02d.sqlite',
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        now.getHours(),
        now.getMinutes(),
        now.getSeconds()
      );

      filename = 'neopi.sqlite';

      let database = new Sequelize('neopi', 'ashlyn', 'I<3science!', {
        host:        'localhost',
        dialect:     'sqlite',
        pool:        {
          max:  5,
          min:  0,
          idle: 10000
        },
        storage:     path.join(__dirname, '../data/', filename), // ':memory',
        transaction: Sequelize.Transaction.DEFERRED, // 'DEFERRED', 'IMMEDIATE', 'EXCLUSIVE'
        benchmark:   true,
        logging:     false
      });

      return database.authenticate().then(() => {
        return loadModels(database).then(() => {
          resolve(database);
        });
      });
    }).catch(err => {
      reject(err);
    });
  });
}

module.exports = {
  loadPriority:  1000,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next) {
    api.database = {};

    api.log(`Database startup`);

    api.Sequelize = Sequelize;

    initDatabase().then(database => {
      api.database = database;
    }).then(() => {
      api.log(`Database connection established`);
      next();
    }).catch(err => {
      api.log(err, 'crit');
      next(err);
    });
  },
  start: function(api, next) {
    next();
  },
  stop: function(api, next) {
    next();
  }
};
