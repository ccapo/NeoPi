'use strict';

const Program = require('../lib/armature/Program').Program;

const programs = [
//  new Program('zero',          0.5, 0.5),
//  new Program('left_top',      1.0, 0.0),
    new Program('left_center',   1.0, 0.5),
//  new Program('left_bottom',   1.0, 1.0),
    new Program('center',        0.5, 0.5),
//  new Program('center_bottom', 0.5, 1.0),
//  new Program('right_top',     0.0, 0.0),
    new Program('right_center',  0.0, 0.5),
//  new Program('right_bottom',  0.0, 1.0),
    new Program('center_top',    0.5, 0.0),
    new Program('center_bottom', 0.5, 1.0)
];

let currentProgram = 0;

exports.task = {
  name: 'cycleArmatureProgram',
  description: 'My Task',
  frequency: 10000,
  queue: 'payload',
  middleware: [],

  run: function (api, params, next) {
//    return next();

    if (!api.armature) return next();

    // Get the next program
    let program = programs[currentProgram];
    // Run the program and take a photo
    api.armature.runProgram(program).then(imageFileName => {
      // Increment and wrap currentProgram
      currentProgram++;
      if (currentProgram >= programs.length) {
        currentProgram = 0;
      }
      next();
    }).catch(err => {
      next(err);
    });
  }
};
