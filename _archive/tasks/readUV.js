'use strict';

exports.task = {
  name: 'readUV',
  description: 'My Task',
  frequency: 2500,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.Ultraviolet) return next();

    let start = Date.now();
    Promise.all([
      api.sensors.Ultraviolet.readUV(),
      // api.sensors.Ultraviolet.readVisible(),
      // api.sensors.Ultraviolet.readIR(),
      // api.sensors.Ultraviolet.readProx(),
    ]).then(result => {
      let uv = result[0];
      // let visible = result[1];
      // let ir = result[2];
      // let prox = result[3];

      // /api.log(`UV Index: ${uv}`);
      return api.sensors.Ultraviolet.store({ uv, /* visible, ir, prox */ }).then(() => {
        // api.log(`Done reading UV`);
        Object.assign(api.readings, {
          uv
        });
        api.log(`Read UV (${Date.now() - start} ms)`);
        next();
      });
    }).catch(err => {
      api.log(`Failed UV read (${err.message}) (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
