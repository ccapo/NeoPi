'use strict';

exports.task = {
  name: 'receiveRadioData',
  description: 'My Task',
  frequency: 0,
  queue: 'default',
  middleware: [],

  run: function(api, params, next) {
    // api.log(`[RECV]`, 'notice', params);
    let room;
    let reading;

    try {
      reading = Object.assign({}, params);
      room = `/sensor/${params.name.toLowerCase()}`;

      delete reading.name;

      api.log(`${room}:`, 'notice', params);
    } catch (e) {
      console.log(params);
      console.trace(e);
    }

    api.chatRoom.broadcastAsync({
      room: api.config.xbee.name
    }, room, reading
    ).then(() => {
      next();
    }).catch(err => {
      next(err);
    });
  }
};
