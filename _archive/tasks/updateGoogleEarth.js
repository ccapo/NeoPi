'use strict';

const fs = require('fs');
const path = require('path');

let home = {
  name: 'Location 1',
  category: 'NeoPi',
  street: 'Unknown',
  lat: 43.3832340,
  lng: -80.3107940,
  altitude: 0
};

let data = null;

const file = path.resolve(__dirname, '../data', 'neopi.kml');

exports.task = {
  name: 'updateGoogleEarth',
  description: 'Given a set of latitude/longitude coordinates, update Google Earth KML document.',
  frequency: 0,
  queue: 'missioncontrol',
  middleware: [],

  run: function (api, params, next) {
    // your logic here
    // let error = new Error('something has gone wrong')
    // let resultLogMessage = {taskResult: 'ok'}
    // next(error, resultLogMessage)
    const GeoJSON = require('geojson');
    const ToKML = require('tokml');

    if (!data) {
      data = [home];
    } else {
      let prev = data[data.length - 1];
      let entry = {
        name: `Location ${data.length + 1}`,
        category: 'NeoPi',
        street: 'Unknown',
        lat: prev.lat * 1.00001,
        lng: prev.lng * 1.00001,
        altitude: prev.altitude + 10
      };
      data.push(entry);

      api.log(`Adding point ${entry.lat},${entry.lng}`);
    }

    let result = GeoJSON.parse(data, {
      Point: ['lat', 'lng'],
      TimeStamp: new Date()
    });

    let kml = ToKML(result);

    fs.writeFile(file, kml, err => {
      if (err) {
        console.trace(err);
      }
      next(err);
    });
  }
};
