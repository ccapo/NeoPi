/*
'use strict'

const fs = require('fs');

exports.task = {
  name: 'readImage',
  description: 'My Task',
  frequency: 0,
  queue: 'default',
  middleware: [],

  run: function (api, params, next) {
    const Camera = require('neopi').Camera;
    const camera = new Camera({
      output: './tmp',
      width: 640,
      height: 480,
      rotation: 90
    });
    let start = Date.now();
    camera.capture().then(result => {
      let image = fs.readFileSync(result);
      let buffer = new Buffer(image).toString('base64');
      let end = Date.now();
      api.chatRoom.broadcast({ room: 'system' }, '/sensor/camera', buffer, function() {
        api.log(`Image captured to ${result} (${end - start} ms)`, 'notice');
        next();
      });
    }).catch(err => {
      console.trace(err);
      next();
    });
    next();
  }
};



*/
