'use strict';

const Promise = require('bluebird');
const whileLoop = require('promise-while-loop');

function dequeueSensorReadings(api) {
  return new Promise((resolve, reject) => {
    let readings = [];
    return whileLoop(function() {
      // predicate
      return api.cache.listLengthAsync('readings').then(length => {
        return length > 0;
      });
    }, function(lastResult) {
      // func
      if (lastResult) {
        readings.push(lastResult);
      }
      return api.cache.popAsync('readings');
    }).then(() => {
      resolve(readings);
    }).catch(err => {
      reject(err);
    });
  });
}

function writeSensorReadings(api) {
  return dequeueSensorReadings(api).then(readings => {
    let ops = [];
    // for (const entry of readings) {
    //   ops.push(api.tasks.enqueueAsync('transmitRadioData', entry, 'default'));
    // }
    return Promise.all(ops).then(() => {
      return api.database.transaction(t => {
        let ops = [];
        for (const reading of readings) {
          let sensor = api.sensors[reading.name];
          if (sensor) {
            ops.push(sensor.storeReading(t, reading));
          } else {
            console.log(`No name for ${reading.name}`);
          }
        }
        return Promise.all(ops);
      });
    });
  });
}

exports.task = {
  name: 'writeSensorReadings',
  description: 'My Task',
  frequency: 10000,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    let start = new Date();
    return api.cache.listLengthAsync('readings').then(length => {
      return writeSensorReadings(api).then(() => {
        let end = Date.now() - start;
        let rps = Math.round(length / (end / 1000));
        api.log(`Wrote ${length} sensor readings (${end} ms, ${rps} rec/s)`, 'notice');
        next();
      }).catch(err => {
        console.trace(err);
        next(err);
      });
    });
  }
};
