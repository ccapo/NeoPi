'use strict';

exports.task = {
  name: 'readTemperature',
  description: 'My Task',
  frequency: 2500,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.Temperature) return next();

    let start = Date.now();
    Promise.all([
      api.sensors.Temperature.read()
    ]).then(result => {
      let temperature = result[0].temperature;
      // api.log(`Temperature: ${temperature} C`);
      return api.sensors.Temperature.store({ temperature }).then(() => {
        // api.log(`Done reading Temperature`);
        Object.assign(api.readings, {
          external_temp: temperature
        });
        api.log(`Read Temperature (${Date.now() - start} ms)`);
        next();
      });
    }).catch(err => {
      api.log(`Failed Temperature read (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
