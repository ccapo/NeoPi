'use strict';

let sprintf = require('sprintf-js').sprintf;

exports.task = {
  name: 'displayReadings',
  description: 'My Task',
  frequency: 1000,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    return next();
    try {
      api.log(sprintf('IR: %03d  UV: %03d  HEAD: %03.2f  TEMP: %-3.1f C  HUM: %3.1f  PRES: %04.1f kPa  ALT: %5.1f m  LAT/LON: %3.3f %3.3f',
        api.readings.ir,
        api.readings.uv,
        api.readings.heading,
        api.readings.temperature,
        api.readings.humidity,
        api.readings.pressure,
        api.readings.altitude,
        api.readings.latitude,
        api.readings.longitude
      ));
    } catch (e) {
      // Not reading yet
      // console.log(e);
    }
    next();
  }
};
