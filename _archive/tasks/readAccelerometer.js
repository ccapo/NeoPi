'use strict';

exports.task = {
  name: 'readAccelerometer',
  description: 'My Task',
  frequency: 2500,
  queue: 'payload',
  middleware: [],

  run: function(api, params, next) {
    if (!api.sensors.Accelerometer) return next();

    let start = Date.now();
    Promise.all([
      api.sensors.Accelerometer.read()
    ]).then(result => {
      let accel = result[0];
      // /api.log(`UV Index: ${uv}`);
      let data = {
        accelX: accel.x,
        accelY: accel.y,
        accelZ: accel.z
      };
      return api.sensors.Accelerometer.store(data).then(() => {
        // api.log(`Done reading Accelerometer`);
        Object.assign(api.readings, data);
        api.log(`Read Accelerometer (${Date.now() - start} ms)`);
        next();
      });
    }).catch(err => {
      console.trace(err);
      api.log(`Failed Accelerometer read (${err.message}) (${Date.now() - start} ms)`, 'error');
      next();
    });
  }
};
