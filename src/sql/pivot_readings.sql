SELECT
    r.timestamp                                        AS timestamp,
    s.name                                             AS sensorName,
    rp.reading_id                                      AS readingId,
    rp.property_id                                     AS propertyId,
    rp.value                                           AS value,
    p.id                                               AS propertyId,
    p.name                                             AS propertyName,
    CASE WHEN p.name = "heading"     THEN rp.value END AS heading,
    CASE WHEN p.name = "x"           THEN rp.value END AS x,
    CASE WHEN p.name = "y"           THEN rp.value END AS y,
    CASE WHEN p.name = "z"           THEN rp.value END AS z,
    CASE WHEN p.name = "uv"          THEN rp.value END AS uv,
    CASE WHEN p.name = "temperature" THEN rp.value END AS temperature,
    CASE WHEN p.name = "humidity"    THEN rp.value END AS humidity,
    CASE WHEN p.name = "pressure"    THEN rp.value END AS pressure,
    CASE WHEN p.name = "altitude"    THEN rp.value END AS altitude,
    CASE WHEN p.name = "visibility"  THEN rp.value END AS visibility,
    CASE WHEN p.name = "ir"          THEN rp.value END AS ir,
    CASE WHEN p.name = "accelX"      THEN rp.value END AS accelX,
    CASE WHEN p.name = "accelY"      THEN rp.value END AS accelY,
    CASE WHEN p.name = "accelZ"      THEN rp.value END AS accelZ,
    CASE WHEN p.name = "lat"         THEN rp.value END AS latitude,
    CASE WHEN p.name = "lon"         THEN rp.value END AS longitude
FROM
    reading_property rp
JOIN
    property p ON p.id = rp.property_id
JOIN
    reading r ON rp.reading_id = r.id
JOIN
            sensor s ON r.sensor_id = s.id
-- WHERE p.name = "temperature"
