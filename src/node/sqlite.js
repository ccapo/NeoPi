'use strict';

const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

const Op = Sequelize.Op;

/**
 *
 * @param cfg
 * @return {Promise<void>}
 */
async function initializeSql(cfg) {
  const Sequelize = require('sequelize');
  const database = new Sequelize(cfg.db, cfg.user, cfg.pass, cfg.options);
  await database.authenticate();
  await loadModels(database);
  return database;
}

/**
 *
 * @param db
 * @return {Promise<void>}
 */
async function loadModels(db) {
  let baseDir = path.join(__dirname, '../../lib/model/');
  let files = fs.readdirSync(baseDir);
  for (const file of files) {
    let filepath = path.join(baseDir, file);
    if (fs.lstatSync(filepath).isFile()) {
      console.log(`Loading ${filepath}`);
      db.import(path.join(__dirname, '../../lib/model/', file));
    }
  }

  let models = Object.keys(db.models);

  for (const model of models) {
    await db.models[model].sync();
  }
}

async function generateData(db, count) {
  let start = Date.now();
  let t;
  try {
    // Start the database transaction
    t = await db.transaction();

    // Find or create the sensor record
    let sensor = await db.models.sensor.findOrCreate({
      where:       {
        name: 'Test Sensor'
      },
      defaults:    {
        name: 'Test Sensor'
      },
      transaction: t
    }).spread((result, created) => {
      return result.get({plain: true});
    });

    // Find or create the reading record
    let reading = await db.models.reading.create({
      timestamp: new Date(),
      sensor_id: sensor.id
    }, { transaction: t }).then(result => {
      return result.get({plain: true});
    });

    // Find or create the property record
    let property = await db.models.property.findOrCreate({
      where: {
        name: 'Test Property'
      },
      defaults: {
        name: 'Test Property'
      },
      transaction: t
    }).spread((result, created) => {
      return result.get({plain: true});
    });

    // Create the reading_property record
    let readingProperty = await db.models.reading_property.create({
      reading_id: reading.id,
      property_id: property.id,
      value: Math.random()
    }, { transaction: t }).then((result, created) => {
      return result.get({plain: true});
    });

    await t.commit();
  } catch (e) {
    console.trace(e);
    if (t) {
      await t.rollback();
    }
  }
  let end = Date.now() - start;
  console.log(`Total time: ${end} ms`);

  // console.log(sensor);
  // console.log('\n\n');
  //
  //
  // console.log(reading);
  // console.log('\n\n');
  //
  //
  // console.log(property);
  // console.log('\n\n');
  //
  // console.log(readingProperty);
  // console.log('\n\n');

 // return reading;
}

/**
 *
 */
(async () => {
  try {
    let dbpath = path.join(__dirname, '../../data/', 'test.sqlite');
    console.log(dbpath);
    let db = await initializeSql({
      db:      'neopi',
      user:    'ashlyn',
      pass:    'I<3science!',
      options: {
        host:        'localhost',
        dialect:     'sqlite',
        operatorsAliases: Op,
        pool:        {
          max:  5,
          min:  0,
          idle: 10000
        },
        storage:     dbpath, // ':memory',
        transaction: Sequelize.Transaction.DEFERRED, // 'DEFERRED', 'IMMEDIATE', 'EXCLUSIVE'
        benchmark:   true,
        logging:     console.log
      }
    });

    await generateData(db, 5);
  } catch (e) {
    console.trace(e);
  }
})();
