const Buffer = require('buffer').Buffer;
const i2c = require('i2c-bus');
const MPL3115A2 = 0x60;
const cmdControlRegister1 = 0x26;
const cmdWhoAmI = 0x0C;
const cmdDataConfig = 0x13;

const regPressure = 0x01;
const regTemperature = 0x04;

let bus = i2c.openSync(1);

function sleep(milliseconds) {
  var dt = new Date();
  while ((new Date()) - dt <= milliseconds) { /* Do nothing */ }
}

// Who Am I?
let whoAmI = bus.readByteSync(MPL3115A2, cmdWhoAmI);

// Set oversampling rate of 128
let setting = bus.readByteSync(MPL3115A2, cmdControlRegister1);
let newSetting = setting | 0x38;
bus.writeByteSync(MPL3115A2, cmdControlRegister1, newSetting);

// Enable event flags
bus.writeByteSync(MPL3115A2, cmdDataConfig, 0x07);

// Toggle one-shot
setting = bus.readByteSync(MPL3115A2, cmdControlRegister1);
if ((setting & 0x02) === 0) {
  bus.writeByteSync(MPL3115A2, cmdControlRegister1, (setting | 0x02))
}

// Read sensor data
let status = bus.readByteSync(MPL3115A2, 0x00);
let count = 0;
let maxCount = 1000;
while ((status & 0x08) === 0 && (count < maxCount)) {
  status = bus.readByteSync(MPL3115A2, 0x00);
  sleep(500);
  count++;
}

// Read pressure
let pBuf = Buffer.alloc(3);
let pBufBytesRead = bus.readI2cBlockSync(MPL3115A2, regPressure, 3, pBuf);

// Read temperature
let tBuf = Buffer.alloc(2);
let tBufBytesRead = bus.readI2cBlockSync(MPL3115A2, regTemperature, 2, tBuf);

// Get status
status = bus.readByteSync(MPL3115A2, 0x00);
console.log(`Status:      0x${status.toString(16)}`);

let pressure = (pBuf[0] << 10) | (pBuf[1] << 2) | (pBuf[2] >> 6)
pressure += ((pBuf[2] & 0x30) >> 4)/4.0;

let celsius = tBuf[0] + (tBuf[1] >> 4)/16.0;
let fahrenheit = (celsius * 9)/5 + 32;

console.log(`Pressure:    ${pressure / 1000} kPa`);
console.log(`Temperature: ${celsius} C (${fahrenheit} F)`);

bus.closeSync();
