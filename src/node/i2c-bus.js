var i2c = require('i2c-bus'),
  wire = i2c.openSync(1);

var MPL3115A2 = 0x60,
  CMD_CTRL_REG = 0x26,
  CMD_ACCESS_CONFIG = 0xac,
  CMD_READ_TEMP = 0xaa,
  CMD_START_CONVERT = 0xee;

//function toCelsius(rawTemp) {
//  var halfDegrees = ((rawTemp & 0xff) << 1) + (rawTemp >> 15);
//
//  if ((halfDegrees & 0x100) === 0) {
//    return halfDegrees / 2; // Temp +ve
//  }
//
//  return -((~halfDegrees & 0xff) / 2); // Temp -ve
//}

(function () {
  //var rawTemp;

  try {
    var whoAmI = wire.readByteSync(MPL3115A2, 0x0C);
    console.log(whoAmI);

  } catch(err) {
    console.trace(err);
  }

  // Enter one shot mode (this is a non volatile setting)
  //i2c1.writeByteSync(MPL3115A2, CMD_ACCESS_CONFIG, 0x01);
  //
  //// Wait while non volatile memory busy
  //while (i2c1.readByteSync(MPL3115A2, CMD_ACCESS_CONFIG) & 0x10) {
  //}
  //
  //// Start temperature conversion
  //i2c1.sendByteSync(MPL3115A2, CMD_START_CONVERT);
  //
  //// Wait for temperature conversion to complete
  //while ((i2c1.readByteSync(MPL3115A2, CMD_ACCESS_CONFIG) & 0x80) === 0) {
  //}
  //
  //// Display temperature
  //rawTemp = i2c1.readWordSync(MPL3115A2, CMD_READ_TEMP);
  //console.log('temp: ' + toCelsius(rawTemp));
  //
  //i2c1.closeSync();
}());
