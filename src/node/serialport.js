'use strict';

const Bluebird = require('bluebird');
const SerialPort = require('serialport');

let xbeeSerial = Bluebird.promisifyAll(new SerialPort('/dev/ttyAMA0', {
  baudRate: 115200
}));

xbeeSerial.on('open',  function() {
  console.log('XBee port open');

  xbeeSerial.writeAsync('hello from blue').then(() => {
    console.log('done writing');
  }).catch(err => {
    console.trace(err);
  });

});

xbeeSerial.on('error', function(err) {
  console.trace('Error: ', err.message);
});

xbeeSerial.on('data', function (buffer) {
  console.log('Data:', buffer.toString('utf8'));
  xbeeSerial.write('ACK\n');
});

xbeeSerial.on('readable', function () {
  console.log('Readable:', xbeeSerial.read());
});
