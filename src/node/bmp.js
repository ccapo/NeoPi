'use strict';

const fs = require('fs');
const gm = require('gm').subClass({imageMagick: true});
const bmp = require('bmp-js');
const util = require('util');
const readFileAsync = util.promisify(fs.readFile);

const STRIDE = 4;
const DEFAULT_PALETTE_INDEX = 4;

class Bitmap {
  findPaletteIndex(palette, r, g, b) {
    for (let i = 0; i < palette.length; i++) {
      if (palette[i].red === r && palette[i].green === g && palette[i].blue === b) {
        return i;
      }
    }

    return DEFAULT_PALETTE_INDEX;
  }

  getPixel(buffer, w, h, x, y) {
    let offset = y * w + x * STRIDE;
    return {
      r: buffer[offset + 3],
      g: buffer[offset + 2],
      b: buffer[offset + 1],
      a: buffer[offset]
    };
  }
};



(async () => {
  let infile = 'images/hab.jpg';
  let outfile = 'images/hab.bmp';
  gm(infile)
    .resizeExact(320, 120)
    .colors(256)
    .write(outfile, async (err) => {
      if (err) {
        console.error(err.toString());
        return;
      }

      let buffer = await readFileAsync(outfile);
      let bitmap = bmp.decode(buffer);

      let w = bitmap.width;
      let h = bitmap.height;
      let offset = 0;

      let packetBufferLength = w * h + bitmap.palette.length * 3;
      let outBuffer = Buffer.alloc(packetBufferLength);

      // Save palette data
      for (let p = 0; p < bitmap.palette.length; p++) {
        outBuffer[offset++] = bitmap.palette[p].red;
        outBuffer[offset++] = bitmap.palette[p].green;
        outBuffer[offset++] = bitmap.palette[p].blue;
      }

      // Save pixel data
      for (let y = 0; y < bitmap.height; y++) {
        for (let x = 0; x < bitmap.width; x++) {
          let pixel = getPixel(bitmap.data, w, h, x, y);
          outBuffer[offset++] = findPaletteIndex(
            bitmap.palette,
            pixel.r,
            pixel.g,
            pixel.b
          );
        }
      }

      return outBuffer;

      // console.log(bitmap);
      // console.log(outBuffer[768]);
      // console.log(bitmap.palette[outBuffer[768]]);
    });
})();
