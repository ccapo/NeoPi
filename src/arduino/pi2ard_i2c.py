i2c_addr = 0x4
import smbus as smbus
i2c = smbus.SMBus(1)

def i2cwrite(str):
  i2c.write_byte(i2c_addr, 0x02)
  for i in map(ord, list(str)):
    i2c.write_byte(i2c_addr, i)
  i2c.write_byte(i2c_addr, 0x03)

i2cwrite("Hello, world!")
