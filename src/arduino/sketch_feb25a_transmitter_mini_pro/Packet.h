#ifndef __PACKET_H
#define __PACKET_H

class Packet {
public:
  Packet() {
    this->Text = NULL;
    this->Length = 0;
  }

  ~Packet () {
    this->Cleanup();
  }

  void Set(char* text){
    this->Cleanup();
    this->Length = strlen(text);
    this->Text = malloc(this->Length);
    memset(this->Text, 0, this->Length);
    strcpy(this->Text, text);
  }

private:
  void Cleanup() {
    if (this->Length > 0) {
      free(this->Text);
      this->Text = NULL;
      this->Length = 0;
    }
  }

public:
  char* Text;
  int Length;
};

#endif

