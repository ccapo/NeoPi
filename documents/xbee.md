##### Coordinator

```
ATNI Blue
ATHP 0
ATID 7FFF
ATCE 1
ATBD 7
ATAP 0
ATAO 0
ATDH 13a200
ATDL 415b3ca7
ATWR
```

ATRE
ATWR
ATHP0,ID7FFF,CE1,BD7,AP0,AO0,DH13a200,DL415b3ca7

##### Endpoint

```
ATNI Green
ATHP 0
ATID 7FFF
ATCE 2
ATBD 7
ATAP 0
ATAO 0
ATDH 13a200
ATDL 415b3c95
ATWR
```

* Set Preamble ID to 0
* Set network ID to 7FFF
* Set the routing/messaging mode (0=router, 1=coordinator, 2=endpoint)
* (option?) Set destination address (high and low)
* Set node identifier
