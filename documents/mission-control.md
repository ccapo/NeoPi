
* Accelerometer
  * -x / +x
  * -y / +y
  * -z / +z
  
* Compass
  * heading
  
* Environment
  * temperature
  * humidity
  * pressure
  * altitude
 
* Luminosity
  * visibility
  * infrared

* Ultraviolet
  * uv

* System
  * CPU
  * Memory
  * Disk I/O

* Armature
  * program 1
  * program 2

* GPS
  * timestamp
  * fix (t/f)
  * latitude
  * longitude
  * altitude
  * speed
  * angle
  * satellites

* Camera
  * image

* Temperature
  * external temp

* Application
  * uptime
  * ascent vector
  * 