
| Sensor   | Type          | Interface | Address | Notes      |
|----------|---------------|-----------|---------|------------|
| DS180B20 | Temperature   | 1-Wire    |         | Thermistor |
| Arduino  | Controller    | I2C       | 0x04    |            |
| HMC5883L | Compass       | I2C       | 0x1E    |            |
| LIS3DH   | Accelerometer | I2C       | 0x18    |            |
| TSL2591  | Luminosity    | I2C       | 0x29    |            |
| SI1145   | Ultraviolet   | I2C       | 0x60    |            |
| BME280   | Environment   | I2C       | 0x77    |            |
