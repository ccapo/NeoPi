'use strict';
const { api, Task } = require('actionhero');
let sprintf = require('sprintf-js').sprintf;

module.exports = class MyTask extends Task {
  constructor() {
    super();
    this.name = 'displayReadings';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 1000 : 0;
    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      api.log(sprintf('IR: %03d  UV: %03d  HEAD: %03.2f  TEMP: %-3.1f C  HUM: %3.1f  PRES: %04.1f hPa  ALT: %5.1f m  LAT/LON: %3.3f %3.3f',
        api.readings.ir,
        api.readings.uv,
        api.readings.heading,
        api.readings.temperature,
        api.readings.humidity,
        api.readings.pressure,
        api.readings.altitude,
        api.readings.latitude,
        api.readings.longitude
      ));
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
