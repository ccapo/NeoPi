'use strict';
const {api, Task} = require('actionhero');

module.exports = class WriteSensorReadingTask extends Task {
  constructor() {
    super();
    this.name = 'writeSensorReading';
    this.description = 'an actionhero task';
    // this.frequency = process.env.MODE === 'PAYLOAD' ? 10000 : 0;
    this.frequency  = 1000;
    this.queue = 'default';
    this.middleware = [];
  }

  async dequeueSensorReadings() {
    let readings = [];
    while (await api.cache.listLength('readings') > 0) {
      readings.push(await api.cache.pop('readings'));
    }
    return readings;
  }

  async writeSensorReadings() {
    try {
      let readings = await this.dequeueSensorReadings();
      for (const reading of readings) {
        let sensor = api.sensors[reading.name];
        if (sensor) {
          await sensor.store_v3(reading);
        } else {
          console.log(`No name for ${reading.name}`);
        }
      }
    } catch (e) {
      throw e;
    }
  }

  async run() {
    try {
      let start = new Date();
      let length= await api.cache.listLength('readings');
      await this.writeSensorReadings();
      let end = Date.now() - start;
      let rps = Math.round(length / (end / 1000));
      if (length === 0) return;
      api.log(`Wrote ${length} sensor readings (${end} ms, ${rps} rec/s)`, 'notice');
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
