'use strict';
const ActionHero = require('actionhero');

module.exports = class MyTask extends ActionHero.Task {
  constructor() {
    super();
    this.name = 'updateGoogleEarth';
    this.description = 'an actionhero task';
    this.frequency = 0;
    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    // your logic here
  }
};
