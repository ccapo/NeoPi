'use strict';
const { api, Task } = require('actionhero');

module.exports = class ReadAccelerometerTask extends Task {
  constructor() {
    super();
    this.name = 'readAccelerometer';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 5000 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      // let start = Date.now();
      let accel = await api.sensors.Accelerometer.read();

      let data = {
        accelX: accel.x,
        accelY: accel.y,
        accelZ: accel.z
      };

      await api.sensors.Accelerometer.store(data);
      Object.assign(api.readings, data);
      // api.log(`Read Accelerometer (${Date.now() - start} ms)`);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
