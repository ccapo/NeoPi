'use strict';
const { api, Task } = require('actionhero');

module.exports = class ReadUvTask extends Task {
  constructor() {
    super();
    this.name = 'readUV';
    this.description = 'an actionhero task';
    this.frequency = process.env.MODE === 'PAYLOAD' ? 5000 : 0;

    this.frequency = 0;

    this.queue = 'default';
    this.middleware = [];
  }

  async run(data) {
    try {
      let start = Date.now();

      let readings = {
        uv: await api.sensors.Ultraviolet.readUV(),
        visible: await api.sensors.Ultraviolet.readVisible(),
        ir: await api.sensors.Ultraviolet.readIR(),
        prox: await api.sensors.Ultraviolet.readProx(),
      };

     /* console.log(`--------------------------------------------`);
      console.log(`UV`);
      console.log(JSON.stringify(readings, null, 2));
      console.log(`--------------------------------------------`);*/

      await api.sensors.Ultraviolet.store(readings);
      Object.assign(api.readings, {
        readings
      });

      // api.log(`Read UV (${Date.now() - start} ms)`);
    } catch (e) {
      api.log(this.name, 'error', e);
    }
  }
};
