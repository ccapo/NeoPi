'use strict';

describe('NeoPi Camera Test', function () {
  this.timeout(30000);

  before(function () {

  });

  after(function () {

  });

  beforeEach(function () {

  });

  afterEach(function () {

  });

  /***************************************************************************/

  it('should be able to talk to the camera', function (done) {
    const Camera = require('../../lib/Camera').Camera;
    let camera = new Camera('../../images');
    camera.capture().then(result => {
      console.log(result);
    }).then(done).catch(done);

  });

});
