'use strict';

const Sensor = require('../core/Sensor').Sensor;

class BMP180 extends Sensor {
  constructor(options) {
    super(options);
  }
}

module.exports.BMP180 = BMP180;
