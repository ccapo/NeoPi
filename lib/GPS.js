'use strict';

const Sensor = require('../core/Sensor').Sensor;

class GPS extends Sensor {
  constructor(options) {
    super(options);
  }
}

module.exports.GPS = GPS;
