'use strict';

// See wiring diagram at https://learn.adafruit.com/adafruits-raspberry-pi-lesson-11-ds18b20-temperature-sensing?view=all

const Sensor = require('../core/Sensor').Sensor;
const Thermistor = require('ds18b20-raspi');

class DS18B20 extends Sensor {
  constructor(options) {
    super(options);
    this.address = 0;
    this.name = 'DS18B20';
  }

  _init() {
    return Promise.resolve();
  }

  read() {
    return new Promise((resolve, reject) => {
      Thermistor.readSimpleC((err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve({
            temperature: result
          });
        }
      });
    });
  }
}

module.exports.DS18B20 = DS18B20;
