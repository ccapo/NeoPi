'use strict';

const Sensor = require('../core/Sensor').Sensor;

const fs       = require('fs');
const path     = require('path');
const RaspiCam = require('raspicam');
const Moment   = require('moment');
const MODE     = 'photo';
const WIDTH    = 3280;
const HEIGHT   = 2464;
const QUALITY  = 100;
const ROTATION = 0;

class Camera extends Sensor {
  constructor(options) {
    super(options);
    this._output = options.output;
    this._width = options.width || WIDTH;
    this._height = options.height || HEIGHT;
    this._quality = options.quality || QUALITY;
    this._rotation = options.rotation || ROTATION;
  }

  capture() {
    return new Promise((resolve, reject) => {
      try {
        let now = Moment().format('YYYYMMDD-HHmmss.SSS');
        let filename = `image-${now}.jpg`;
        let filepath = path.resolve(path.join(this._output, filename));
        let opts = {
          mode:      MODE,
          output:    filepath,
          width:     this._width,
          height:    this._height,
          quality:   this._quality,
          rotation:  this._rotation,
          vstab:     true,
          sharpness: 100,
          nopreview: true,
          log:       function(message) {
            console.log(message);
          }
        };
        let camera = new RaspiCam(opts);

        camera.on('start', () => {
          console.log(`[RASPICAM] Start capture to ${filepath}...`);
        });

        camera.on('read', () => {
          //console.log(`[RASPICAM] Reading...`);
        });

        camera.on('stop', () => {
          console.log(`[RASPICAM] Stopped.`);
        });

        camera.on('exit', () => {
          console.log(`[RASPICAM] Exited.`);
          resolve(filepath);
        });

        camera.start();
      } catch (e) {
        reject(e);
      }
    });
  }
}

module.exports.Camera = Camera;

/*
 var piexif = require("piexif.js");
 var fs = required("fs");

 var jpeg = fs.readFileSync(filename1);
 var data = jpeg.toString("binary");
 var exifObj = piexif.load(data);
 exifObj["GPS"][piexif.GPSIFD.GPSVersionID] = [7, 7, 7, 7];
 exifObj["GPS"][piexif.GPSIFD.GPSDateStamp] = "1999:99:99 99:99:99";
 var exifbytes = piexif.dump(exifObj);
 var newData = piexif.insert(exifbytes, data);
 var newJpeg = new Buffer(newData, "binary");
 fs.writeFileSync(filename2, newJpeg);
*/
