'use strict';

const Program = require('../lib/armature/Program').Program;

exports['default'] = {
  armature: (api) => {
    if (process.env.MODE === 'MISSIONCONTROL') return null;

    return {
      programs: [
    //  new Program('zero',          0.5, 0.5),
    //  new Program('left_top',      1.0, 0.0),
        new Program('left_center',   1.0, 0.5),
    //  new Program('left_bottom',   1.0, 1.0),
        new Program('center',        0.5, 0.5),
    //  new Program('center_bottom', 0.5, 1.0),
    //  new Program('right_top',     0.0, 0.0),
        new Program('right_center',  0.0, 0.5),
    //  new Program('right_bottom',  0.0, 1.0),
        new Program('center_top',    0.5, 0.0),
        new Program('center_bottom', 0.5, 1.0)
      ]

    };
  }
};
